from cetr_helper import *
import argparse


def cetr(URL, scraper):
    print(URL)
    if scraper != 'bs4':
        html = get_html_from_url_sel(URL)
    else:
        html = get_html_from_url_bs4(URL)
    html_lines = clean_html(html)
    ttr_ratios = get_smoothed_ttr(html_lines)
    der_list = derivative(ttr_ratios)
    data = pd.DataFrame({"smoothed_ttr": ttr_ratios, "derivative": der_list})
    hist_1 = plot_hist_one(data)
    hist_2 = plot_hist_two(data)
    plot_1 = plot_scatter_one(data)
    centroids, labels = k_means(data)
    print(centroids)
    plot_2 = plot_scatter_two(data, labels)
    labels_to_consider = centroid_finder(centroids)
    plot_3 = plot_scatter_three(data, labels, labels_to_consider)
    start, end = index_finder(labels_to_consider, labels)
    req_html = html_lines[start:end + 1]
    req_html_string = '\n'.join(req_html)
    soup = BeautifulSoup(req_html_string, 'lxml')
    req_text = soup.get_text().split('\n')
    req_text = list(filter(None, req_text))
    temp_df = pd.DataFrame({"label": labels, "html_content": html_lines})
    df_out = pd.concat([data, temp_df], axis=1)
    df_test = df_out[df_out['label'].isin(labels_to_consider)]
    test_html_string = '\n'.join(list(df_test['html_content']))
    test_soup = BeautifulSoup(test_html_string, 'lxml')
    test_req_text = test_soup.get_text().split('\n')
    test_req_text = list(filter(None, test_req_text))
    return plot_1, plot_2, plot_3, hist_1, hist_2, test_html_string

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='extract main text content from webpage using tag ratios')
    parser.add_argument('URL', help="URL to perform CETR on")
    args = parser.parse_args()
    cetr(args.URL)
